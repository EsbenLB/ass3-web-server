# MovieDb API #
An Entity framework code first created database with ASP.NET Core Web API.  

# Contributors #
Esben Bjarnason (https://gitlab.com/EsbenLB)
Stian Selle (https://gitlab.com/stianstian95)

## Table of Contents
- [About](#about)
- [Installation](#installation)
- [Issues](#issues)

## About ##
The first part of this project can be found primarily in the Models>Domain folder
and MovieDbContext file. Here we create a database with 3 tables (Movies, characters, franchises)
and seed them with some data. We only specify the modeldomains navigation links and 
EF handles the rest. Movies and characters are a many-to-many relationship while franchise
and movies is an one-to-many relationship.

Second part of this project can firstly be found in the controllers folder, where the
endpoints or CRUD operations are located. To encapsulate the data model away from the user
we use DTOs so the user can e.g. post a movie without specifying any character or franchise.
To make the endpoints more readable we use swagger to document them. We also somewhat utilize
services, though only for a limit purpose there would be no problem to rewrite the controllers
to use them explicetely.

## Installation ##
Clone repository and open with Visual Studio.

Install following tools:

* Visual Studio 2019 with .NET 5
* SQL Server Managment Studio
* Tools -> NutGet... -> Manage NutGet packages ... -> Add Package
  - // Version = latest 5, = 5.0.14  
  - Microsoft.EntityFrameworkCore.Design  
  - Microsoft.entityFrameworkCore.SqlServer  
  - Microsoft.entityFrameworkCore.Tools  
  - AutoMapper.Extensions.Microsoft.DependencyInjection  
  - Swashbuckle.AspNetCore // Swagger  


Connect to local server (change this line inn appsettings.json"):

    "DefaultConnection": "Data Source = <<NAME OF CONNECTION STRING>>; Initial Catalog = MovieCharactersDb; Integrated Security = True;"  

Tools -> NutGet... -> ... Console  
   update-database

F5 to run

## Issues  ##  
If error config file. Try to restart visual studio or checkout Sas3 branch instead (older version).  
// Sometimes the config file gets added to git commit mess.