
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MovieAPIDemo.Models.DTO.Character;
using MovieAPIDemo.Models.DTO.Movie;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Controllers
{
    /// <summary>
    /// API end points for movie.
    /// </summary>
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    [Route("[controller]")]
    public class MoviesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(MovieDbContext context, IMapper mapper, IMovieService movieService)
        {
            _context = context;
            _mapper = mapper;
            _movieService = movieService;
        }
        /// <summary>
        /// Get all movies.
        /// </summary>
        /// <returns>All movies</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var movies = await _context.Movies.Include(m => m.Characters).ToListAsync();

            List<MovieReadDTO> movieRead = _mapper.Map<List<MovieReadDTO>>(movies);

            return movieRead;
        }
        /// <summary>
        /// Get movie by Id
        /// </summary>
        /// <param name="id">Get movie by Id</param>
        /// <returns>Movie</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovieById(int id)
        {
            var movie = await _context.Movies.Include(f => f.Characters).Where(m => m.Id == id).FirstOrDefaultAsync();

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Get characters in a movie
        /// </summary>
        /// <param name="id">Primary key of movie</param>
        /// <returns>Characters</returns>
        [HttpGet("character/{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetCharactersByMovie(int id)
        {
            var movie = await _context.Movies.Include(c => c.Characters).FirstOrDefaultAsync(c => c.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(movie.Characters.ToList());                                    
        }

        /// <summary>
        /// Add movie to database
        /// </summary>
        /// <param name="movie">Movie to add to the database</param>
        /// <returns>Api controller action</returns>
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie([FromBody] MovieCreateDTO movie)
        {
            Movie domainMovie = _mapper.Map<Movie>(movie);

            try
            {
                _context.Movies.Add(domainMovie);

                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            MovieReadDTO newMovie = _mapper.Map<MovieReadDTO>(domainMovie);

            return CreatedAtAction("GetMovieById", new {Id = newMovie.Id}, newMovie);
        }

        /// <summary>
        /// Delete movie by Id
        /// </summary>
        /// <param name="id">Delete character by Id</param>
        /// <returns>StatusCode 204</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Update movie by Id
        /// </summary>
        /// <param name="id">Update movie with Id</param>
        /// <param name="editedMovie">Updated movie to replace old movie</param>
        /// <returns>StatusCode 204</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutMovie(int id, MovieEditDTO editedMovie)
        {
            if (id != editedMovie.Id)
            {
                return BadRequest();
            }

            Movie domainMovie = _mapper.Map<Movie>(editedMovie);
            _context.Entry(domainMovie).State = EntityState.Modified;
            //var movies = await _context.Movies.Include(m => m.Characters).ToListAsync();


            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Update character list in Movie
        /// </summary>
        /// <param name="id">Update movie with Id</param>
        /// <param name="characters">Updated character list for mvoie</param>
        /// <returns>StatusCode 204</returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> characters)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _movieService.UpdateMovieCharactersAsync(id, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid character.");
            }

            return NoContent();
        }


    }
}
