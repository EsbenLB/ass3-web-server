﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPIDemo.Models.DTO.Character;
using MovieAPIDemo.Models.DTO.Franchise;
using MovieAPIDemo.Models.DTO.Movie;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTO.Franchise;
using MovieCharactersAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Controllers
{
    /// <summary>
    /// API end points for franchise.
    /// </summary>
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    [Route("[controller]")]
    public class FranchiseController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchiseController(MovieDbContext context, IMapper mapper, IFranchiseService franchiseService)
        {
            _context = context;
            _mapper = mapper;
            _franchiseService = franchiseService;
        }
        /// <summary>
        /// Get all franchises.
        /// </summary>
        /// <returns>All franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            var franchises = await _context.Franchises.Include(c => c.Movies).ToListAsync();

            List<FranchiseReadDTO> franchiseRead = _mapper.Map<List<FranchiseReadDTO>>(franchises);

            return franchiseRead;
        }
        /// <summary>
        /// Get Franchise by Id
        /// </summary>
        /// <param name="id">Id for franchise</param>
        /// <returns>Franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies).Where(m => m.Id == id).FirstOrDefaultAsync();

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }
        /// <summary>
        /// Add franchise to database.
        /// </summary>
        /// <param name="franchise">Franchise to add to database</param>
        /// <returns>Return api controller end action</returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise([FromBody] FranchiseCreateDTO franchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);

            try
            {
                _context.Franchises.Add(domainFranchise);

                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            FranchiseReadDTO newFranchise = _mapper.Map<FranchiseReadDTO>(domainFranchise);

            return CreatedAtAction("GetFranchiseById", new { Id = newFranchise.Id }, newFranchise);
        }
        /// <summary>
        /// Delete franchise by Id
        /// </summary>
        /// <param name="id">Franchise to delete by Id</param>
        /// <returns>statuscode 204</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Update franchise by Id
        /// </summary>
        /// <param name="id">Update franchise by Id</param>
        /// <param name="editedFranchise">Updated franchise to replace old franchise</param>
        /// <returns>Statuscode 204</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutFranchise(int id, FranchiseEditDTO editedFranchise)
        {
            if (id != editedFranchise.Id)
            {
                return BadRequest();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(editedFranchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Update all movies in franchise.
        /// </summary>
        /// <param name="id">Franchise to update</param>
        /// <param name="movies">New list for franchise with all movies with this franchise</param>
        /// <returns>Statscode 204</returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, List<int> movies)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.UpdateFranchiseMoviesAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid franchise.");
            }

            return NoContent();
        }

        /// <summary>
        /// Get Characters in franchise
        /// </summary>
        /// <param name="id">Get by franchise id</param>
        /// <returns>Character in franchise</returns>
        [HttpGet("{id}/characters")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetCharactersByFranchise(int id)
        {
            var franchise = await _context.Franchises.Include(c => c.Movies).ThenInclude(m => m.Characters).FirstOrDefaultAsync(c => c.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }
            HashSet<CharacterReadDTO> characters = new();
            List<int> characterIds = new();
            foreach (Movie movie in franchise.Movies)
            {
                foreach (Character character in movie.Characters)
                {
                    if (characterIds.Contains(character.Id))
                    {
                        continue;
                    }
                    characterIds.Add(character.Id);
                    var CharacterReadDTO = _mapper.Map<CharacterReadDTO>(character);
                    characters.Add(CharacterReadDTO);
                }
            }
            return Ok(characters);
        }
        /// <summary>
        /// Get Movies from franchise
        /// </summary>
        /// <param name="id">Get by franchise id</param>
        /// <returns>Movies in franchise</returns>
        [HttpGet("movies/{id}")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesByFranchise(int id)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies).ThenInclude(m => m.Characters).FirstOrDefaultAsync(f => f.Id == id);
            if (franchise == null)
            {
                return NotFound();
            }
            var movies = franchise.Movies;
            var movieDTO = _mapper.Map<List<MovieReadDTO>>(movies);
            return Ok(movieDTO);
        }

    }
}
