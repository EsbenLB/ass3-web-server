﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPIDemo.Models.DTO.Character;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Controllers
{
    /// <summary>
    /// API end points for character.
    /// </summary>
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    [Route("[controller]")]
    public class CharacterController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;
        
        public CharacterController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all characters.
        /// </summary>
        /// <returns>All characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            var characters = await _context.Characters.Include(c => c.Movies).ToListAsync();

            List<CharacterReadDTO> characterRead = _mapper.Map<List<CharacterReadDTO>>(characters);

            return characterRead;
        }
        /// <summary>
        /// Get character by ID.
        /// </summary>
        /// <param name="id">Id for character</param>
        /// <returns>Charater specificed by Id</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterById(int id)
        {
            var character = await _context.Characters.Include(f => f.Movies).Where(m => m.Id == id).FirstOrDefaultAsync();

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }
        /// <summary>
        /// Add Character to database.
        /// </summary>
        /// <param name="character">Character to add</param>
        /// <returns>Return api controller action</returns>
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter([FromBody] CharacterCreateDTO character)
        {
            Character domainCharacter = _mapper.Map<Character>(character);

            try
            {
                _context.Characters.Add(domainCharacter);

                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            CharacterReadDTO newCharacter = _mapper.Map<CharacterReadDTO>(domainCharacter);

            return CreatedAtAction("GetCharacterById", new { Id = newCharacter.Id }, newCharacter);
        }
        /// <summary>
        /// Delete character by ID
        /// </summary>
        /// <param name="id">Id for character you want to delete</param>
        /// <returns>StatusCode 204, No content</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Update character by Id.
        /// </summary>
        /// <param name="id">Id for character you want to update</param>
        /// <param name="editedCharacter">Updated character to replace old character</param>
        /// <returns>StatusCode 204, No content</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutCharacter(int id, CharacterEditDTO editedCharacter)
        {
            if (id != editedCharacter.Id)
            {
                return BadRequest();
            }

            Character domainCharacter = _mapper.Map<Character>(editedCharacter);
            _context.Entry(domainCharacter).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
