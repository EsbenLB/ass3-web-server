﻿using AutoMapper;
using MovieAPIDemo.Models.DTO.Movie;
using MovieCharactersAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    /// <summary>
    /// Automapping for Movie DTOs
    /// </summary>
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>().ForMember(pdto => pdto.Characters, opt => opt.MapFrom(p => p.Characters.Select(s => s.Id).ToArray())).ReverseMap();
            CreateMap<Movie, MovieCreateDTO>().ReverseMap();
            CreateMap<Movie, MovieEditDTO>().ReverseMap();
        }
    }
}
