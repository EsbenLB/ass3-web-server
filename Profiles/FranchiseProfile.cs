﻿using AutoMapper;
using MovieAPIDemo.Models.DTO.Franchise;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTO.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    /// <summary>
    /// Automapping for Franchise DTOs
    /// </summary>
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>().ForMember(pdto => pdto.Movies, opt => opt.MapFrom(p => p.Movies.Select(s => s.Id).ToArray())).ReverseMap();
            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseEditDTO>().ReverseMap();
        }
    }
}
