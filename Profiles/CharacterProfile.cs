﻿using AutoMapper;
using MovieAPIDemo.Models.DTO.Character;
using MovieCharactersAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    /// <summary>
    /// Automapping for Character DTOs
    /// </summary>
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>().ForMember(pdto => pdto.Movies, opt => opt.MapFrom(p => p.Movies.Select(s => s.Id).ToArray())).ReverseMap();
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();
            CreateMap<Character, CharacterEditDTO>().ReverseMap();
        }
    }
}
