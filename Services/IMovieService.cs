﻿using MovieCharactersAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Services
{
    /// <summary>
    /// Service interface for database manipulation of Franchise
    /// </summary>
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetSpecificMovieAsync(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);
        public Task UpdateMovieCharactersAsync(int movieId, List<int> characters);
        public Task UpdateMovieFranchiseAsync(int movieId, int franchise);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
    }
}
