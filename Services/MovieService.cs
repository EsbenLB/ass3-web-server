﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Services
{
    /// <summary>
    /// Service/repository for database manipulation of Franchise
    /// </summary>
    public class MovieService : IMovieService
    {
        private readonly MovieDbContext _context;
        public MovieService(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Method to add a new Movie to the database
        /// </summary>
        /// <param name="movie">A Movie object</param>
        /// <returns>Asynchronous task to add the movie</returns>
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        /// <summary>
        /// Method for deleting a movie from the databse
        /// </summary>
        /// <param name="id">primary key for the movie</param>
        /// <returns>Asynchronous task for deleting the movie</returns>
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets all the movies
        /// </summary>
        /// <returns>List of movies</returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(c => c.Franchise)
                .Include(c => c.Characters)
                .ToListAsync();
        }

        /// <summary>
        /// Gets a movie by primary key
        /// </summary>
        /// <param name="id">the primary key for the movie</param>
        /// <returns>the Task to get the movie</returns>
        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }

        /// <summary>
        /// Checks to see if a specified movie exists
        /// </summary>
        /// <param name="id">primary key for movie</param>
        /// <returns>True of false depending on success</returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        /// <summary>
        /// Updates the movie with new fields
        /// </summary>
        /// <param name="movie">movie object</param>
        /// <returns>async operation</returns>
        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates characters in movie
        /// </summary>
        /// <param name="movieId">movie to be configured</param>
        /// <param name="characters">characters to be added</param>
        /// <returns>async operation</returns>
        public async Task UpdateMovieCharactersAsync(int movieId, List<int> characters)
        {
            Movie movieToUpdateChar = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.Id == movieId)
                .FirstAsync();

            List<Character> chars = new();
            foreach (int charId in characters)
            {
                Character charac = await _context.Characters.FindAsync(charId);
                if (charac == null)
                    
                    throw new KeyNotFoundException();
                chars.Add(charac);
            }
            movieToUpdateChar.Characters = chars;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates movie in franchise
        /// </summary>
        /// <param name="movieId">id of movie to be added</param>
        /// <param name="franId">franchise to be added to</param>
        /// <returns>async operation</returns>
        public async Task UpdateMovieFranchiseAsync(int movieId, int franId)
        {
            Movie movieToUpdateFran = await _context.Movies
                .Include(c => c.Franchise)
                .Where(c => c.Id == movieId)
                .FirstAsync();

            Franchise fran = await _context.Franchises.FindAsync(franId);

            if (fran == null)
                throw new KeyNotFoundException();

            movieToUpdateFran.Franchise = fran;
            await _context.SaveChangesAsync();
            
        }

        
    }
}
