﻿using MovieCharactersAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Services
{
    /// <summary>
    /// Service interface for database manipulation of Franchise
    /// </summary>
    public interface IFranchiseService
    {
        public Task UpdateFranchiseMoviesAsync(int moiveId, List<int> movies);
        public bool FranchiseExists(int id);
    }
}
