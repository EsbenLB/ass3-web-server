﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Services
{
    /// <summary>
    /// Service/repository for database manipulation of Franchise
    /// </summary>
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;
        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Checks to see if a franchise exists
        /// </summary>
        /// <param name="id">primary key for the franchise you want to find</param>
        /// <returns>true or false depending on success</returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }

        /// <summary>
        /// Method to "give" a franchise a list of films
        /// </summary>
        /// <param name="franchiseId">Primary key for the franchise</param>
        /// <param name="movies">List of movies to be added</param>
        /// <returns>An asynchronous operation</returns>
        public async Task UpdateFranchiseMoviesAsync(int franchiseId, List<int> movies)
        {
            Franchise franchiseToUpdateMovie = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.Id == franchiseId)
                .FirstAsync();

            List<Movie> movie = new();
            foreach (int movieId in movies)
            {
                Movie tmpMovie = await _context.Movies.FindAsync(movieId);
                if (tmpMovie == null)

                    throw new KeyNotFoundException();
                movie.Add(tmpMovie);
            }
            franchiseToUpdateMovie.Movies = movie;
            await _context.SaveChangesAsync();
        }
    }
}
