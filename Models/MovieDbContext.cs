


using MovieCharactersAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;

namespace MovieCharactersAPI.Models {

    /// <summary>
    /// Where the database with tables and seeded data is created/configured
    /// </summary>
    public class MovieDbContext : DbContext 
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public MovieDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // m2m relationship, add seeded data
            modelBuilder.Entity<Movie>()
               .HasOne(m => m.Franchise)
               .WithMany(f => f.Movies)
               .IsRequired(false);

            modelBuilder.Entity<Character>()
                .HasMany(m => m.Movies)
                .WithMany(c => c.Characters)
                .UsingEntity<Dictionary<string, object>>(
                "CharacterMovie",
                b => b.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                c => c.HasOne<Character>().WithMany().HasForeignKey("CharacterId"));
            //table =>
            //{
            //    table.HasKey("MovieId", "CharacterId");
            //    table.HasData(
            //        new { movieId = 2, characterId = 2 },
            //        new { movieId = 2, characterId = 3 },
            //        new { movieId = 3, characterId = 1 },
            //        new { movieId = 4, characterId = 1 }
            //        );
            //}

            // Seed data
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 1, MovieTitle = "Nissen 2", 
                Genre = "Action", ReleaseYear = 1945, Director = "Bob", Picture = "nicePic", 
                Trailer = "trailer", FranchiseId = 1});
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 2,
                MovieTitle = "Nissen 3",
                Genre = "Action",
                ReleaseYear = 1946,
                Director = "Bob 2",
                Picture = "nicePic 2",
                Trailer = "trailer 3",
                FranchiseId = 1,
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 3,
                MovieTitle = "Nissen 4",
                Genre = "Action",
                ReleaseYear = 1947,
                Director = "Bob 3",
                Picture = "nicePic 3",
                Trailer = "trailer 3",
                FranchiseId = 1,
                Characters = new List<Character>()
            });
            // Character
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 1,
                Name = "bob",
                Alias = "Not bob",
                Gender = "Boy",
                Picture = "Pic of bob",
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 2,
                Name = "bob 2",
                Alias = "Not bob 2",
                Gender = "Boy",
                Picture = "Pic of bob 2",
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 3,
                Name = "bob 3",
                Alias = "Not bob 3",
                Gender = "Boy",
                Picture = "Pic of bob 3",
            });
            // Franchise
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 1,
                Name = "Action",
                Description = "Its cool action",
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 2,
                Name = "Super Action",
                Description = "Its Super cool action!",
            });
        }
    }
}
