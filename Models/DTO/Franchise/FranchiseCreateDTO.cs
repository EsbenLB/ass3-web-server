using System.Threading.Tasks;

namespace MovieAPIDemo.Models.DTO.Franchise
{
    public class FranchiseCreateDTO
    {
        /// Data transfer object for Franchise creation model
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
