﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTO.Franchise
{
    public class FranchiseEditDTO
    {
        /// Data transfer object for Franchise update model
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
