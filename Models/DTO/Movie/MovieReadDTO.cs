using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieAPIDemo.Models.DTO.Movie
{
    public class MovieReadDTO
    {
        /// Data transfer object for Movie read model
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public int FranchiseId { get; set; }
        public List<int> Characters { get; set; }
    }
}
