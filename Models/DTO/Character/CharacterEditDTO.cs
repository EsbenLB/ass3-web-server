using System.Threading.Tasks;

namespace MovieAPIDemo.Models.DTO.Character
{
    /// Data transfer object for Character update model
    public class CharacterEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
    }
}
