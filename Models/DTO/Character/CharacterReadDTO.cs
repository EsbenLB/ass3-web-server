using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieAPIDemo.Models.DTO.Character
{
    /// Data transfer object for Character read model
    public class CharacterReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        public List<int> Movies { get; set; }
    }
}
